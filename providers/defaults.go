package providers

const (
	Aws   = "aws"
	Azure = "azure"
)

type Provider interface {
	GetValue(key string) (*string, error)
}
