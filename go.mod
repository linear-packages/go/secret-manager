module gitlab.com/linear-packages/go/secret-manager

go 1.20

require github.com/aws/aws-sdk-go v1.42.4

require github.com/jmespath/go-jmespath v0.4.0 // indirect
