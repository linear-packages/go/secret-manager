package sm

import (
	"os"
	"strings"

	"gitlab.com/linear-packages/go/secret-manager/providers"
)

type SecretManager struct {
	providerName string
}

func NewSecretManager() *SecretManager {
	p := os.Getenv("SECRET_PROVIDER")
	if strings.TrimSpace(p) == "" {
		p = providers.Aws // Default
	}
	return &SecretManager{
		providerName: p,
	}
}

func (sm *SecretManager) GetValue(key string) (*string, error) {
	s := sm.getService()
	return s.GetValue(key)
}

func (sm *SecretManager) getService() providers.Provider {
	switch sm.providerName {
	case providers.Aws:
		return &providers.AwsSSM{}
	default:
		return nil
	}
}
